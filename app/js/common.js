$(function () {
	//E-mail Ajax Send
	$('.default-form').submit(function (e) {
		var th = $(this);
		var formData = new FormData($(this)[0]);
		$.ajax({
			url: 'scripts/mail.php',
			type: 'POST',
			data: formData,
			async: false,
			success: function (msg) {
				alert(msg);
				setTimeout(function () {
					$.fancybox.close(); //Закрыть pop-up
					// Done Functions
					th.trigger("reset");
				}, 1000);
			},
			error: function (msg) {
				alert('Ошибка!');
			},
			cache: false,
			contentType: false,
			processData: false
		});
		e.preventDefault();
	});
	$(".calculate-form").submit(function (e) {
		var formData = new FormData($(this)[0]);
		var th = $(this);
		$.ajax({
			url: 'scripts/mail.php',
			type: "POST",
			data: formData,
			async: false,
			success: function (msg) {
				$(".tab-content-item").removeClass("active");
				$("#tab-three").addClass("active");
				$(".tab-nav li").removeClass("active");
				$(".tab-nav li[link=\"tab-three\"]").addClass("active");
				setTimeout(function () {
					// Done Functions
					th.trigger("reset");
				}, 1000);
			},
			error: function (msg) {
				alert('Ошибка!');
			},
			cache: false,
			contentType: false,
			processData: false
		});
		e.preventDefault();
	});

	//Animation
	$(".animation_1").animated("flipInY", "fadeOutDown");
	$(".animation_2").animated("fadeInUp", "fadeOutDown");
	$(".animation_3").animated("fadeInLeft", "fadeOutDown");
	$(".animation_4").animated("fadeInRight", "fadeOutDown");

	// Hover effect advantage-item
	$(".advantage-item").hover(function () {
		$(".advantage-item").removeClass("active");
		$(this).addClass("active");
	});

	// Hover effect product
	$(".product-item").hover(function () {
		$(".product-item").removeClass("active");
		$(this).addClass("active");
	});

	//Shipping Animation

	var shippingElem = document.getElementById('shipping-animate');

	var shippingSourceBottom = shippingElem.getBoundingClientRect().top + window.pageYOffset;

	window.onscroll = function () {
		if (shippingElem.classList.contains('active') && window.pageYOffset < shippingSourceBottom) {
			shippingElem.classList.remove('active');
			$('.line-shipping').children("img").removeClass('active');
		} else if (window.pageYOffset > shippingSourceBottom) {
			shippingElem.classList.add('active');
			setTimeout(addActiveClass, 1000);
		}
	};

	function addActiveClass() {
		$('.line-shipping').children("img").addClass('active');
	}

	// Slider Home-page
	$('.owl-home').owlCarousel({
		loop: true,
		nav: false,
		items: 1,
		autoplay: true,
		smartSpeed: 1000,
		autoplayHoverPause: true
	})

	//Masked input
	$("input[type=\"tel\"]").mask("+7-999-999-9999");

	// Tab form
	$(".tab-nav li").click(function () {
		$(".tab-nav li").removeClass("active");
		$(this).addClass("active");

		var link = $(this).attr("link");

		$(".tab-content-item").removeClass("active");
		$("#" + link).addClass("active");
	});
	$("#next-tab").click(function () {
		var link = $(this).closest(".tab-content-item").attr("id");
		$(".tab-nav li[link=\"" + link + "\"]").removeClass("active");
		$(".tab-nav li[link=\"" + link + "\"]").next().addClass("active");

		$(this).closest(".tab-content-item").removeClass("active");
		$(this).closest(".tab-content-item").next().addClass("active");
		return false;
	});

	// Upload file
	$(".file-attach-btn input[type=file]").change(function () {
		var filename = $(this).val().replace(/.*\\/, "");
		$("#filename").val(filename);
	});

	//Slow scroll
	$('a[href^="#"]').on('click', function (event) {
		event.preventDefault();
		var sc = $(this).attr("href"),
			dn = $(sc).offset().top;
		$('html, body').animate({ scrollTop: dn }, 1000);
	});

	//Initialization Fancybox
	$('[data-fancybox]').fancybox({
		animationEffect: 'fade',
		beforeShow: function () {
			$("body *:not(.fancybox-container, .fancybox-container *)").addClass("blur");
		},
		afterClose: function () {
			$("body *:not(.fancybox-container, .fancybox-container *)").removeClass("blur");
		}
	});

	//Yandex map
	if (document.getElementById('ya-map')) {
		ymaps.ready(init);
	}
	function init() {
		var myMap = new ymaps.Map("ya-map", {
			center: [53.208149, 44.995526],
			zoom: 15
		});

		var myPlacemark = new ymaps.Placemark([53.208149, 44.995526], {
			hintContent: 'Бриск',
			balloonContent: 'Ул. Коммунистическая 28 - 419'
		}, {
				iconColor: 'black'
			});

		myMap.geoObjects.add(myPlacemark);
	}

});